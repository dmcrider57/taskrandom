package com.dayloncrider.android.taskrandom;


import android.content.Context;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

class RandomTask {
    //Private variables
    private String task;
    private Context context;

    //Constructor
    RandomTask(Context context){
        this.context = context;
        this.task = getNewTask();
    }

    String getTask(){
        return this.task;
    }

     private String getNewTask(){
        String newTask;
        ArrayList<String> myList = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open("tasks.txt")));
            String line;
            while((line = reader.readLine()) != null){
                myList.add(line);
            }

            //Pick a random task from the list
            newTask = getRandomItem(myList);

        } catch (IOException e) {
            //log the exception
            newTask = e.toString();
        }

        return newTask;
    }

    private String getRandomItem(ArrayList<String> list){
        Random rand = new Random();
        String taskItem = list.get(rand.nextInt(list.size()));

        TaskActivity save = new TaskActivity();
        save.saveCurrentTask(taskItem, true);

        return taskItem;
    }
}
