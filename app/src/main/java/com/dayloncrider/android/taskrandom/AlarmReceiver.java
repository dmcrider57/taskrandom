package com.dayloncrider.android.taskrandom;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class AlarmReceiver extends BroadcastReceiver {
    public static String newTask;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        //This happens when the alarm goes off
        newTask = new RandomTask(context).getTask();

        //Create the notification
        NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
        notification.setSmallIcon(R.drawable.notification_icon);
        notification.setContentTitle(context.getResources().getString(R.string.notification_title));
        notification.setContentText("Your new task: " + newTask);

        //Set when happens when the notification is tapped
        Intent notificationIntent = new Intent(context, TaskActivity.class);
        notificationIntent.putExtra("task",newTask);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        //This next block ensures that hitting the "back" key will exit the application and
        //return the user to the home screen
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(TaskActivity.class);
        //Adds the intent that starts the activity to the top of the stack
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

        notification.setContentIntent(resultPendingIntent);
        notification.setAutoCancel(true);


        //Deploy the notification
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1,notification.build());


    }
}
