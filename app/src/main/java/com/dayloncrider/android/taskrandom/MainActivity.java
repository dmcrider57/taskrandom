package com.dayloncrider.android.taskrandom;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {
    public static final String SHARED_PREFS = "TaskRandomSharedPreferences";
    //For the AlarmManager
    Intent intentAlarm;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            //Start a new Intent of TaskActivity
            startActivity(new Intent(MainActivity.this,TaskActivity.class));
        }

        boolean alarmUp = (PendingIntent.getBroadcast(this, 0,
                new Intent(this, AlarmReceiver.class),
                PendingIntent.FLAG_NO_CREATE) != null);

        if(alarmUp){
            startActivity(new Intent(MainActivity.this,TaskActivity.class));
        }

    }

    @Override
    protected void onResume(){
        super.onResume();
        //Update the textView
        SharedPreferences saveData = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        String current_text = saveData.getString("currentTask",null);
        if(current_text != null){
            startActivity(new Intent(MainActivity.this,TaskActivity.class));
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);

        //Start a new Intent of TaskActivity
        startActivity(new Intent(MainActivity.this,TaskActivity.class));
    }

    public void scheduleAlarm(View v)
    {
        // The time at which the alarm will be scheduled.
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(GregorianCalendar.HOUR_OF_DAY,8);
        cal.set(GregorianCalendar.MINUTE,0);
        cal.set(GregorianCalendar.SECOND,0);
        Long time = cal.getTimeInMillis();

        // Create an Intent and set the class that will execute when the Alarm triggers. Here we have
        // specified AlarmReceiver in the Intent. The onReceive() method of this class will execute when the broadcast from your alarm is received.
        intentAlarm = new Intent(this, AlarmReceiver.class);
        intentAlarm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getBroadcast(this, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get the Alarm Service.
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        // Set the alarm for a particular time.
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, 1000*60*60*24,pendingIntent);
        Toast.makeText(this, "Congrats! You'll get daily reminders until you tap 'END' in the app.", Toast.LENGTH_LONG).show();
    }

    public void startTasks(View v){
        scheduleAlarm(v);
        startActivity(new Intent(MainActivity.this,TaskActivity.class));
    }

}