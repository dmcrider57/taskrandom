package com.dayloncrider.android.taskrandom;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TaskActivity extends AppCompatActivity {
    public static final String SHARED_PREFS = "TaskRandomSharedPreferences";
    SharedPreferences saveData;
    private TextView current_task;
    @Override
    protected void onResume(){
        super.onResume();
        //Update the textView
        saveData = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        String current_text = saveData.getString("currentTask",null);
        if(current_text != null){
            current_task.setText(current_text);
        }else{
            updateTextView();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        if(savedInstanceState != null){
            //Get the saved value for currentTask and populate
            current_task.setText(savedInstanceState.getString("currentTask"));
        }

        RandomTask rTask = new RandomTask(this);
        current_task = (TextView) findViewById(R.id.current_task);
        current_task.setText(rTask.getTask());
        makeLinkClickable();
    }

    public void endTasks(View v){
        Intent intentAlarm = new Intent(this, AlarmReceiver.class);
        intentAlarm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        stopService(intentAlarm);
        alarmManager.cancel(pendingIntent);
        Toast.makeText(this, "You will no longer get daily reminders unless you hit 'START' again.", Toast.LENGTH_LONG).show();
        saveCurrentTask(null,false);
        startActivity(new Intent(TaskActivity.this,MainActivity.class));
        finish();
    }

    public void updateTextView(){
        Intent notificationIntent = getIntent();
        String notificationText = notificationIntent.getStringExtra("task");
        if(notificationText != null){
            current_task.setText(notificationText);
            saveCurrentTask(notificationText, true);
        }
    }

    public void saveCurrentTask(String text, Boolean save){
        if(saveData != null) {
            SharedPreferences.Editor editor = saveData.edit();
            if (save) {
                editor.putString("currentTask", text);
            } else {
                editor.clear();
            }
            editor.apply();
        }
    }
    public void makeLinkClickable(){
        TextView submitTask = (TextView) findViewById(R.id.submittask);
        submitTask.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
